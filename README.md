# RC Circuits Lab

So far, we've only seen steady state, or time-independent circuits.  In this lab, we will see what happens in a time-dependent circuit.

# Setup

Windows users will need to install `python3-tk`. Open `Ubuntu 22.04.2 LTS` and run:

`sudo apt install python3-tk`

For all users, open Codium and press `ctrl + shift + p` (`cmd` for mac users) to open the command pallet. Then type `git clone`, press `enter`, and then type `https://gitlab.com/wuphysics87/rc-circuits-lab` to clone this repository. Save it in your repos directory.

Now, we'll need to run some commands in order install the required dependencies. Open your terminal, and from the root of this directory, (you'll know by typing `ls` and you'll be able to see `data.py` and `plots.py`) run: 

`pip3 install -r requirements.txt`

If that doesn't work, try `pip` instead of `pip3` or ask for help.

# Theoretical Analysis

Consider the following RC circuit:

![RC-Circuit](images/RC-circuit-diagram.jpg)

Labeling the upper plate of the capacitor as $+Q$ and the lower plate as $-Q$ and evaluating:

##### $\oint \vec{E} \cdot d\vec{r} =0$

going around a counter-clockwise path, we find that:

##### $IR + \frac{Q}{C} = 0$

Noting that $I$ and $Q$ are related according to: $I = \frac{dQ}{dt}$, we can rewrite this equation as:

##### $R \frac{dQ}{dt} + \frac{Q}{C} = 0$

Isolating $Q$ and integrating:

##### $\int \frac{dQ}{Q} = - \int \frac{dt}{RC}$

we find that:

##### $ln{Q(t)} - ln{Q_0} = - \frac{t}{RC}$

Where $Q_0$ is the charge on the capacitor at time $t=0$. Exponentiating both sides:

##### $Q(t) = Q_0 e^{-\frac{t}{RC}}$

This can be recast as the voltage across the capacitor as a function of time by noting that: 

##### $Q=VC$

So finally:

### $V(t) = V_0 e^{-\frac{t}{RC}}$

# Experimentally Determining C

In this section, we'll be configuring our experimental setup in order to determine the value of our capacitor. First, configure your multimeter as show in order to measure the value of the blue resistor:

![multimeter](images/multimeter.jpeg)

Once you've determined the value of the blue resistor, build the circuit in the solderless breadboard as shown:

![breadboard](images/breadboard.jpeg)

Then connect the breadboard to the Pasco box:

![pasco-box](images/pasco-box.jpeg)

Once you've connected everything, navigate to https://gitlab.com/wuphysics87/rc-circuits-lab/-/blob/main/rc-circuit-template.cap to download the Pasco template to the lab computer. Once it's finished downloading, open it and continue. You should see something similar to the following:

![pasco-template](images/pasco-template.jpeg)

Now you'll be able to determine the capacitor's capacitance. Click the record button.  You may also need to press the trigger button to keep the plots from gittering:

![trigger-button](images/trigger-button.png)

Use the equation from the [Theoretical Analysis Section](#theoretical-analysis), your measured value of the resistance, and the fit parameters to solve for  $C$

# What If? Analysis in Python

In science, we often have the ability to do theoretical analysis as well as take experimental data, but we don't always have the ability to test for all desired parameters in the lab. Now that we have the value of our capacitance, we want to use it to generate plots for hypothetical resistances. 

From Codium, open `data.py` and `plots.py`. Modify `data.py` to generate some plots of hypothetical situations.

To generate your plots, run `plots.py` from your terminal with

`python3 plots.py`


If you get any errors, troubleshoot them using the information provided in the error message. Fixing the error will allow you to generate multiple plots at once!

Once you are finished paste a screenshot of your plot into OneNote


# A Little Bit of Git

We want to use Git in order to see the changes we've made to our repository. From you `CLI` (Command Line Interface), navigate to the root of this repo. 

Type `git log` to see the list of commits that have been made already. You can scroll this list with the arrow keys, or pressing `d` for down or `u` for up. Make note of the style  of the commit messages (length, descriptiveness etc.) . Now type `q` to exit this list.

Now we want to see the changes we've made.  Type `git diff` to see the difference in the files you've changed since the last commit.

Type `git status` to see a list of the files that have changed.  We now want to 'stage' your changes in order to make a new commit. To do this type `git add` followed by the names of the files you'd like to stage.

Now that your changes are staged, it's time to make your own commit. To do so, type 

`git commit -m "YOUR COMMIT MESSAGE"`

Where `YOUR COMMIT MESSAGE` should be short, active tense, and describe what you've done since the last commit.

Once you've made your commit, you'll be able to see it by typing `git log` again.

While the command line is the fastest way to use git, most of git can be used from within Codium as well. Check out

https://code.visualstudio.com/docs/sourcecontrol/overview

to learn more.

# Computing Points 

The following two repositories will gain you computing points for the end of the semester:

- [Hello Bitwarden](https://gitlab.com/wuphysics87/hello-bitwarden)
- [Hello GitLab](https://gitlab.com/wuphysics87/hello-gitlab)

if you time, start Hello Bitwarden now. Otherwise we will work on them on Monday.

