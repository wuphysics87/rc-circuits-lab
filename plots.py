#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import data

# Import data from data.py
t_max=data.t_max
Rs=data.resistors
C=data.capacitor
V_o=data.V_o

# 100 linearly spaced numbers from 0 to tmax
t = np.linspace(0,t_max,100)


# Your function here:

# Setting Plot options
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Loop to plot N resistors
try:
    for R in Rs:
        arg = -R*C
        V = V_o * np.exp(t/arg)
        plt.plot(t,V, label="R = " + str(R) + " Ohms")
        plt.legend(loc='upper right')


        # Axis Labels and Title
        plt.title('RC Circuit Voltage vs. Time')
        plt.ylabel("Voltage (V)")
        plt.xlabel("Time (s)")

        # show the plot
        plt.show()
        plt.savefig('my_plot.png')

except:
    print ("Error! resistors variable in data.py must be a list!")
    print ("See https://www.w3schools.com/python/python_lists.asp")
