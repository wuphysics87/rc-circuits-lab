#!/usr/bin/env python3


# Maximum time in seconds
t_max=100

# Resistors' values. This must be a list.  Look up how to use Python's list data type.
resistors=0

# Capacitor value
capacitor=2

# Initial Voltage
V_o=0
